import { HomePage } from './ngrx/models/homepage.model';

export interface AppState {
  readonly homePage: HomePage[];
}
