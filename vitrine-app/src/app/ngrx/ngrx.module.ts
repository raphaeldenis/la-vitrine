import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppEffects } from './effects/app.effects';
import * as Reducers from './reducers/app.reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  declarations: [],
  imports: [
    FlexLayoutModule,
    StoreModule.forRoot(Reducers.AppState, {
      metaReducers: Reducers.metaReducers
    }),
    EffectsModule.forRoot(AppEffects),
    StoreDevtoolsModule.instrument({
      maxAge: 100
    })
  ]
})
export class NgrxModule {}
