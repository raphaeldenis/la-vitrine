import { Action } from '@ngrx/store';
import { HomePage } from '../models/homepage.model';
import * as HomePageActions from './../actions/homepage.actions';

const initialState: HomePage = {
  id: 0,
  name: 'Profil',
  url: 'profil',
  active: true
};

export function HomePageReducer(state: HomePage[] = [initialState], action: HomePageActions.Actions) {
  switch (action.type) {
    case HomePageActions.SELECT_COMPONENT:
      return [...state, action.payload];
    default:
      return state;
  }
}
