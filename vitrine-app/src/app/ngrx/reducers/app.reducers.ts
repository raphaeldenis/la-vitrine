// import "@ngrx/core/add/operator/select";
import { ActionReducer, MetaReducer } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
// import * as LoginModuleReducers from './login/login.reducers';

export interface IState {
  //   loginModule: LoginModuleReducers.IState;
  // *********************** Modules **************************/
  // router: RouterReducers.IState;
  // *********************** Autres **************************/
  // configuration: ConfigurationReducers.IState;
}

export const AppState = {
  //   loginModule: LoginModuleReducers.reducers
  // *********************** Modules **************************/
  // router: RouterReducers.reducers,
  // *********************** Autres **************************/
  // configuration: ConfigurationReducers.reducers,
};

// Persistance des données dans la localstorage
export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: [
      {
        //     loginModule: {
        //       serialize: LoginModuleReducers.serialize,
        //       deserialize: LoginModuleReducers.deserialize
        //     }
        //   },
        //   {
        //     users: {
        //       serialize: UsersReducers.serialize,
        //       deserialize: UsersReducers.deserialize
        //     }
      }
    ],
    rehydrate: true
  })(reducer);
}

export const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

/*export default compose(combineReducers)({
  TimelineElements: TimelineElementsReducers.reducers
});
*/
