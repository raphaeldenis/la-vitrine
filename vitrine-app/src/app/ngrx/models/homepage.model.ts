import { StringifyOptions } from 'querystring';

export interface HomePage {
  id: number;
  name: string;
  url: string;
  active: boolean;
}
