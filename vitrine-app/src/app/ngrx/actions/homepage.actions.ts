import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { HomePage } from '../models/homepage.model';

export const SELECT_COMPONENT = '[COMPONENT] Select';
export const UNSELECT_COMPONENT = '[COMPONENT] Unselect';

export class SelectComponent implements Action {
  readonly type = SELECT_COMPONENT;

  constructor(public payload: HomePage) {}
}

export class UnselectComponent implements Action {
  readonly type = UNSELECT_COMPONENT;

  constructor(public payload: number) {}
}

export type Actions = SelectComponent | UnselectComponent;
