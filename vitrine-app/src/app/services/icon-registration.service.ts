import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class IconRegistrationService {
  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer) {
    this.iconRegistry.addSvgIcon(
      'home',
      this.sanitizer.bypassSecurityTrustResourceUrl('./assets/images/icons/24-home.svg')
    );
    iconRegistry.addSvgIcon(
      'curly',
      this.sanitizer.bypassSecurityTrustResourceUrl('./assets/images/icons/curly-bracket.svg')
    );
  }
}
