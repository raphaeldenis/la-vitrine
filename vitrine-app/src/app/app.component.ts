import { Component } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { IconRegistrationService } from 'src/app/services/icon-registration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'vitrine-app';

  constructor(private iconRegistrationService: IconRegistrationService) {}
}
