import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

interface Tab {
  id: number;
  label: string;
  url: string;
  active: boolean;
}
@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {
  public menu: Tab[] = [];

  constructor(private router: Router, private route: ActivatedRoute) {
    this.menu = [
      { id: 1, label: 'Profil', url: 'profil', active: true },
      { id: 2, label: 'Parcours', url: 'parcours', active: false },
      { id: 3, label: 'Projets', url: 'projets', active: false },
      { id: 4, label: 'Contact', url: 'contact', active: false }
    ];
  }

  ngOnInit() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((navigationEnd: NavigationEnd) => {
        const currentUrl: string = navigationEnd.urlAfterRedirects.substring(1);
        console.log(currentUrl);
        this.menu = this.updateActiveTab([...this.menu], currentUrl);
      });
  }

  private updateActiveTab(menu: Tab[], currentUrl: string): Tab[] {
    menu.forEach((tab: Tab) => {
      tab.active = tab.url === currentUrl;
    });
    return menu;
  }

  public redirect(tab: Tab) {
    console.log(tab);
    this.router.navigate([tab.url]);
    this.menu.forEach(tabMenu => {
      tabMenu.active = tabMenu.id === tab.id;
    });
  }
}
