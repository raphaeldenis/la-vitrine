import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilComponent } from './profil/profil.component';
import { ParcoursComponent } from './parcours/parcours.component';
import { ProjetsComponent } from './projets/projets.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  {
    path: 'profil',
    component: ProfilComponent,
    data: { title: 'Profil' }
    // canActivate: [AuthGuardAppService]
  },
  {
    path: 'parcours',
    component: ParcoursComponent,
    data: { title: 'Parcours' }
    // canActivate: [AuthGuardAppService]
  },
  {
    path: 'projets',
    component: ProjetsComponent,
    data: { title: 'Projets' }
    // canActivate: [AuthGuardAppService]
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: { title: 'Contact' }
    // canActivate: [AuthGuardAppService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
