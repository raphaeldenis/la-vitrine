import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { ProfilComponent } from './profil/profil.component';
import { ParcoursComponent } from './parcours/parcours.component';
import { ProjetsComponent } from './projets/projets.component';
import { ContactComponent } from './contact/contact.component';
import { MaterialModule } from 'src/app/material/material.module';

@NgModule({
  declarations: [ProfilComponent, ProjetsComponent, ParcoursComponent, ContactComponent],
  imports: [CommonModule, HomeRoutingModule, MaterialModule],
  exports: [ProfilComponent, ProjetsComponent, ParcoursComponent, ContactComponent]
})
export class HomeModule {}
