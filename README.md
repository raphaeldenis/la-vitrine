# projetsPerso (BUSY)

Current project:

VitrineApp:
Interactive resume used as playground for Angular 7 and angular material.

In Progress:
Store: Ngrx,
Flux management: rxjs,
REST: API's both local and external,
Hosted: raspberry pie 3 with Docker

GIT:
Master branch for production,
Working on development branch,
new branch forked from development for new features.

To Test (need NPM):
Clone the repository,
from terminal: npmi ; npm start,
localhost:4200 by default
